package com.websocket;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;

public class WebserverVerticleSingleInstance extends AbstractVerticle{
	
	@Override
	public void start(Future<Void> startFuture) throws Exception {
		super.start(startFuture);
		System.out.println("WebserverVerticleSingleInstance.start()");
		final EventBus eventBus = vertx.eventBus();	
		final Pattern chatUrlPattern = Pattern.compile("/chat/(\\w+)");
		Router router = Router.router(vertx);
		// body handler
		router.route().handler(BodyHandler.create());
		
		router.get("/").handler(rctx -> {
			rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html")
					.sendFile("webroot/chat.html");
		});
		
//		router.get(".*\\.(css|js)$").handler(rctx -> {
//			rctx.response().sendFile("web/" + new File(rctx.request().path()));
//		});
		
		// static content
	    router.route("/*").handler(StaticHandler.create());
		
		vertx.createHttpServer().requestHandler(router::accept).listen(8070);
		
		vertx.createHttpServer().websocketHandler(new Handler<ServerWebSocket>() {

			@Override
			public void handle(ServerWebSocket ws) {
				final Matcher m = chatUrlPattern.matcher(ws.path());
				System.out.println("WebserverVerticleSingleInstance.start(...).new Handler() {...}.handle() m.matches(): "+m.matches());
				if (!m.matches()) {
					ws.reject();
					return;
				}
				
				final String chatRoom = m.group(1);
				final String id = ws.textHandlerID();
				System.out.println("registering new connection with id: " + id + " for chat-room: " + chatRoom);
				
				LocalMap<String, String> localMap = vertx.sharedData().getLocalMap("chat.room." + chatRoom);
				localMap.put(id, chatRoom);
				
				ws.closeHandler(new Handler<Void>() {
					@Override
					public void handle(final Void event) {
						System.out.println("un-registering connection with id: " + id + " from chat-room: " + chatRoom);
						LocalMap<String, String> localMap = vertx.sharedData().getLocalMap("chat.room." + chatRoom);
						localMap.remove(id);
					}
				});
				
				ws.handler(new Handler<Buffer>() {

					@Override
					public void handle(Buffer data) {
						ObjectMapper m = new ObjectMapper();
						try {
							JsonNode rootNode = m.readTree(data.toString());
							((ObjectNode) rootNode).put("received", new Date().toString());
							String jsonOutput = m.writeValueAsString(rootNode);
							System.out.println("json generated: " + jsonOutput + " data.toString():   "+data.toString());
							LocalMap<String, String> localMap = vertx.sharedData().getLocalMap("chat.room." + chatRoom);
							Set<Entry<String, String>> set = localMap.entrySet();
							Iterator<Entry<String, String>> iter = set.iterator();
							while (iter.hasNext()) {
								Map.Entry<String, String> entry = (Map.Entry<String, String>) iter
										.next();
								String chatterId = entry.getKey();
								eventBus.send(chatterId, jsonOutput);
							}
							
						} catch (IOException e) {
							ws.reject();
						}
					}
					
				});
				
			}
			
		}).listen(8090);
	}

}
