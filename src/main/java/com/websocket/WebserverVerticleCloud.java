package com.websocket;

import java.io.IOException;
import java.util.Date;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.shareddata.AsyncMap;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;

public class WebserverVerticleCloud extends AbstractVerticle{
	
	@Override
	public void start(Future<Void> startFuture) throws Exception {
		super.start(startFuture);
		final EventBus eventBus = vertx.eventBus();	
		final Pattern chatUrlPattern = Pattern.compile("/chat/(\\w+)");
		Router router = Router.router(vertx);
		// body handler
		router.route().handler(BodyHandler.create());
		
		router.get("/").handler(rctx -> {
			rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html")
					.sendFile("web/chat.html");
		});
		
//		router.get(".*\\.(css|js)$").handler(rctx -> {
//			rctx.response().sendFile("web/" + new File(rctx.request().path()));
//		});
		
		// static content
	    router.route("/*").handler(StaticHandler.create());
		
		vertx.createHttpServer().requestHandler(router::accept).listen(8070);
		
		vertx.createHttpServer().websocketHandler(new Handler<ServerWebSocket>() {

			@Override
			public void handle(ServerWebSocket ws) {
				final Matcher m = chatUrlPattern.matcher(ws.path());
				if (!m.matches()) {
					ws.reject();
					return;
				}
				
				final String chatRoom = m.group(1);
				final String id = ws.textHandlerID();
				System.out.println("registering new connection with id: " + id + " for chat-room: " + chatRoom);
				vertx.sharedData().<String, Set<String>>getClusterWideMap("chat.room." + chatRoom, resultHandler -> {
					System.out.println("WebserverVerticle.start(...).getClusterWideMap handler.succeeded(): "+resultHandler.succeeded());
					if(resultHandler.succeeded()) {
						AsyncMap<String, Set<String>> map = resultHandler.result();
						map.get(chatRoom, idHandler -> {
							Set<String> idSet = idHandler.result();
							idSet.add(id);
							map.put(chatRoom, idSet, completionHandler -> {
								System.out.println("WebserverVerticle.start(...)putting into getClusterWideMap completionHandler.succeeded(): "+completionHandler.succeeded());
							});
						});
					}
				});
				
				ws.closeHandler(new Handler<Void>() {
					@Override
					public void handle(final Void event) {
						System.out.println("un-registering connection with id: " + id + " from chat-room: " + chatRoom);
						vertx.sharedData().<String, Set<String>>getClusterWideMap("chat.room." + chatRoom, resultHandler -> {
							System.out.println("WebserverVerticle.start(...).closeHandler handler.succeeded(): "+resultHandler.succeeded());
							if(resultHandler.succeeded()) {
								AsyncMap<String, Set<String>> map = resultHandler.result();
								map.get(chatRoom, idHandler -> {
									Set<String> idSet = idHandler.result();
									idSet.remove(id);
									map.put(chatRoom, idSet, completionHandler -> {
										System.out.println("WebserverVerticle.start(...)putting into closeHandler completionHandler.succeeded(): "+completionHandler.succeeded());
									});
								});
							}
						});
					}
				});
				
				ws.handler(new Handler<Buffer>() {

					@Override
					public void handle(Buffer data) {
						ObjectMapper m = new ObjectMapper();
						try {
							JsonNode rootNode = m.readTree(data.toString());
							((ObjectNode) rootNode).put("received", new Date().toString());
							String jsonOutput = m.writeValueAsString(rootNode);
							System.out.println("json generated: " + jsonOutput + " data.toString():   "+data.toString());
							vertx.sharedData().<String, Set<String>>getClusterWideMap("chat.room." + chatRoom, resultHandler -> {
								System.out.println("WebserverVerticle.start(...).getClusterWideMap handler.succeeded(): "+resultHandler.succeeded());
								if(resultHandler.succeeded()) {
									AsyncMap<String, Set<String>> map = resultHandler.result();
									map.get(chatRoom, idHandler -> {
										Set<String> idSet = idHandler.result();
										while (idSet.iterator().hasNext()) {
											String chatterId =  idSet.iterator().next();
											eventBus.send(chatterId, jsonOutput);
										}
									});
								}
							});
						} catch (IOException e) {
							ws.reject();
						}
					}
					
				});
				
			}
			
		}).listen(8090);
	}

}
